<?php

namespace Finpaa\TTP;

use Finpaa\Finpaa;

class OpenPayment
{
    private static $INITIAL_TOKEN;

    private static $initiationAuth;
    private static $institutions;

    private static $AIS;
    // private static $singlePIS;

    const AUTH_REDIRECT = "OAuth2";
    // const PRE_AUTH = 'PRE_AUTH';
    // const EMBEDDED_AUTH = 'EMBEDDED_AUTH';

    private static function selfConstruct()
    {
        $finpaa = new Finpaa();
        $sequences = $finpaa->Sweden()->{env('FINPAA_ENVIRONMENT')}()->OpenPayments();

        self::$initiationAuth = $sequences->InitiationAuth();
        self::$institutions   = $sequences->Institutions();
        self::$AIS = $sequences->AIS();
        self::$PIS = $finpaa->Sweden()->{env('FINPAA_ENVIRONMENT')}()->Open_Payment_PIS()->PIS();
    }

    private static function getSequenceCode($name) {
      if(self::$$name) {
        return self::$$name;
      }
      else {
        self::selfConstruct();
        return self::getSequenceCode($name);
      }
    }

    private static function executeSequenceMethod($code, $methodIndex, $alterations, $name, $returnPayload)
    {
        $sequence = Finpaa::getSequenceMethods($code);

        if (isset($sequence->SequenceExecutions)) {

          if($methodIndex < count($sequence->SequenceExecutions))
          {
            $response = Finpaa::executeTheMethod(
              $sequence->SequenceExecutions[$methodIndex]->methodCode, $alterations, $returnPayload
            );

            return array('error' => false, 'response' => json_decode(json_encode($response), true));
          }
          else {
              return array('error' => true, 'message' => $name . ' method Failed -> Index out of bound',
                'methodIndex' => $methodIndex, 'methodsCount' => count($sequence->SequenceExecutions)
              );
          }
        }
        else {
          return array('error' => true,
            'message' => $name .' -> No sequence executions', 'response' => json_decode(json_encode($sequence), true)
          );
       }
    }

    public static function getInitialToken()
    {
        if(self::$INITIAL_TOKEN)
        {
            return self::$INITIAL_TOKEN;
        }
        else
        {
            $alterations[]['body'] = array(
              "client_id"     => env("OPEN_PAYMENT_CLIENT_ID"),
              "client_secret" => env("OPEN_PAYMENT_CLIENT_SECRET"),
            );

            $response = self::executeSequenceMethod(
              self::getSequenceCode("initiationAuth"), 0, $alterations, 'Initial Token', false
            );

            if(!$response['error'] && isset($response['response']))
                self::$INITIAL_TOKEN = "Bearer " . $response['response']['access_token'];
            else self::$INITIAL_TOKEN = "Unauthorized";

            return self::getInitialToken();
        }
    }

    public static function getCountries()
    {
        $alterations[]['headers'] = array(
          "Accept"                    => null,
          "X-Request-ID"              => GUID(),
          "Authorization"             => self::getInitialToken(),
          "Digest"                    => null,
          "Signature"                 => null,
          "TPP-Signature-Certificate" => null,
        );

        return self::executeSequenceMethod(
          self::getSequenceCode('institutions'), 0, $alterations, 'Institutions', false
        );
    }

    public static function getInstitutions($countryCode)
    {
        $alterations[]['headers'] = array(
          "Accept"                    => null,
          "X-Request-ID"              => GUID(),
          "Authorization"             => self::getInitialToken(),
          "Digest"                    => null,
          "Signature"                 => null,
          "TPP-Signature-Certificate" => null,
        );

        $alterations[]['query'] = array(
          "isoCountryCodes" => $countryCode,
        );

        return self::executeSequenceMethod(
          self::getSequenceCode('institutions'), 1, $alterations, 'Institutions', false
        );
    }

    public static function getInstitutionDetail(string $id)
    {
        $alterations[]['headers'] = array(
          "Accept"                    => null,
          "X-Request-ID"              => GUID(),
          "Authorization"             => self::getInitialToken(),
          "Digest"                    => null,
          "Signature"                 => null,
          "TPP-Signature-Certificate" => null,
        );

        $alterations[]['path'] = array(
          "bicFi" => $id,
        );

        return self::executeSequenceMethod(
          self::getSequenceCode('institutions'), 2, $alterations, 'Institutions', false
        );
    }

    public static function Ais($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('AIS'), $methodIndex, $alterations, 'Open payment AIS', $returnPayload
        );
    }

    public static function Pis($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('PIS'), $methodIndex, $alterations, 'Open payment PIS', $returnPayload
        );
    }
}
